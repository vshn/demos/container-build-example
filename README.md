# VSHN Container Build Example

This repository aims to demonstrate how to build container images with GitLab CI, and push them to the GitLab Registry.

## How it works

In general, building container images on GitLab is a bit of a mess[^1] because the runners themselves run as docker containers (at least ours do for security reasons).
To work around this, you can either make the host's Docker daemon available inside the running job (which is a huge security risk), or start another daemon inside the job container.

The alternative is to use an alternative builder which does not rely on Docker daemon to build images.
[Kaniko](https://github.com/GoogleContainerTools/kaniko) has proven to be the most straight-forward one, but others (such as Buildah) exist.

The drawback of this solution is that you can't rely on features such as `buildx` (for building multi-arch images), but in practice this is rarely needed at VSHN.

Upstream Documentation: <https://docs.gitlab.com/ee/ci/docker/using_kaniko.html>


### Troubleshooting

No trouble encountered yet :fingers_crossed:

---

[^1]: See <https://docs.gitlab.com/ee/ci/docker/using_docker_build.html>
